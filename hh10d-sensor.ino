#include <Wire.h>

unsigned int sensitivity = 0;
unsigned int offset = 0;

unsigned int read2Bytes(byte deviceAddr, byte memAddr) {
  Wire.beginTransmission(deviceAddr);
  Wire.write(memAddr);
  Wire.endTransmission();

  Wire.requestFrom(deviceAddr, 2U);
  while (Wire.available() < 2) {
    delay(10);
  }

  unsigned int val = Wire.read() * 256;
  val += Wire.read();
  return val;
}

void setup() {
  // put your setup code here, to run once:
  pinMode(7, INPUT);
  Wire.begin();
  Serial.begin(9600);

  sensitivity = read2Bytes(81, 10);
  offset = read2Bytes(81, 12);

  Serial.println("Setup done");
}

unsigned int getFrequency() {
  unsigned long mean = 0;
  noInterrupts();
  for (byte i = 0; i < 100; i++) {
    mean += pulseIn(7, HIGH) + pulseIn(7, LOW);
  }
  interrupts();
  return 100000000UL / mean;
}

unsigned int measureHumidity() {
  unsigned int freq = getFrequency();
  //Serial.println(freq);
  unsigned int val = (offset - freq) * sensitivity / 4096;
  return val;
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(measureHumidity());
  delay(200);
}
